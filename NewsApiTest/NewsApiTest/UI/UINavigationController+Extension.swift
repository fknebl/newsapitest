//
//  UINavigationController+Extension.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit

extension UINavigationController {
    
    static func create(rootModule: ModuleCreator) -> UINavigationController {
        UINavigationController(rootViewController: rootModule.create())
    }
    
}
