//
//  ImageFetchService.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import Foundation
import Combine
import Alamofire
import AlamofireImage

protocol ImageFetchServiceProtocol {
    func getImage(from url: String) -> AnyPublisher<UIImage, Error>
}

class ImageFetchService: ImageFetchServiceProtocol {
    let downloader = ImageDownloader(
        configuration: ImageDownloader.defaultURLSessionConfiguration(),
        downloadPrioritization: .fifo,
        maximumActiveDownloads: 4,
        imageCache: AutoPurgingImageCache()
    )
    
    func getImage(from url: String) -> AnyPublisher<UIImage, Error> {
        Future<UIImage, Error>() { [weak self] promise in
            let urlRequest = URLRequest(url: URL(string: url)!)
            self?.downloader.download(urlRequest, completion:  { response in
                switch response.result {
                case .success(let image):
                    promise(.success(image))
                case .failure(let error):
                    promise(.failure(error))
                }
            })
        }.eraseToAnyPublisher()
    }
}
