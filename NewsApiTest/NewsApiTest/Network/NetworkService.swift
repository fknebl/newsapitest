//
//  NetworkService.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 08..
//

import Foundation
import Moya
import Combine

class NetworkService {
    
    func request<T: TargetType>(target: T) -> AnyPublisher<Response, Error> {
        let provider = MoyaProvider<T>(plugins: [NetworkLoggerPlugin()])
        return Future<Response, Error>() { promise in
            provider.request(target) { result in
                switch result {
                case .success(let response):
                    promise(.success(response))
                case .failure(let error):
                    promise(.failure(error))
                }
            }
        }.eraseToAnyPublisher()
    }
    
}
