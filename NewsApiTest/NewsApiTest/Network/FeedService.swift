//
//  FeedService.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import Foundation
import Moya
import Combine

protocol FeedServiceProtocol {
    func getFeed(query: String) -> AnyPublisher<Feed, Error>
}

class FeedService: NetworkService, FeedServiceProtocol {
    func getFeed(query: String) -> AnyPublisher<Feed, Error> {
        request(target: FeedTarget.getFeed(apiKey: Constants.serviceApiKey, query: query)).tryMap { response in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let filteredResponse = try response.filterSuccessfulStatusCodes()
            return try decoder.decode(Feed.self, from: filteredResponse.data)
        }.eraseToAnyPublisher()
    }
}
