//
//  MockFeedService.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 08..
//

import Foundation
import Combine

class MockFeedService: FeedServiceProtocol {
    func getFeed(query: String) -> AnyPublisher<Feed, Error> {
        Just(Feed(status: "ok", totalResults: 2, articles: [
            Article(source: ArticleSource(id: "engadget", name: "Endgadget"), author: "TestAuthor", title: "TestTitle", articleDescription: "TestDescription", url: "http://example.com", urlToImage: nil, publishedAt: Date(), content: "TestContent"),
            Article(source: ArticleSource(id: "techcrunch", name: "TechCrunch"), author: "TestAuthor", title: "TestTitle2", articleDescription: "TestDescription2", url: "http://example.com", urlToImage: "https://g.foolcdn.com/editorial/images/641528/september-stock-market-investment-autumn-fall-growth-returns.jpg", publishedAt: Date(), content: "TestContent")
        ])).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}
