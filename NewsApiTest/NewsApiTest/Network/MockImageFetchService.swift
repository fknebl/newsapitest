//
//  MockImageFetchService.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 08..
//

import UIKit
import Combine

class MockImageFetchService: ImageFetchServiceProtocol {
    
    func getImage(from url: String) -> AnyPublisher<UIImage, Error> {
        Just(UIImage(systemName: "return")!).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
    
}
