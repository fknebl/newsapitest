//
//  FeedTarget.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import Foundation
import Moya

enum FeedTarget {
    case getFeed(apiKey: String, query: String)
}

extension FeedTarget: TargetType {
    var baseURL: URL { URL(string: "https://newsapi.org/v2")! }
    var path: String {
        switch self {
        case .getFeed:
            return "/everything"
        }
    }
    var method: Moya.Method {
        switch self {
        case .getFeed:
            return .get
        }
    }
    var task: Task {
        switch self {
        case .getFeed(let apiKey, let query):
            return .requestParameters(parameters: ["q": query, "apiKey": apiKey], encoding: URLEncoding.queryString)
        }
    }
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
