//
//  ModuleCreator.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit

protocol ModuleCreator {
    func create() -> UIViewController
}

extension UIViewController {
    
    static func instantiateFromStoryboard<T: UIViewController>(storyboard: UIStoryboard) -> T? {
        storyboard.instantiateInitialViewController() as? T
    }
    
}
