//
//  BaseViewController.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit
import Combine

class BaseViewController: UIViewController {
    var disposeBag = Set<AnyCancellable>()
}
