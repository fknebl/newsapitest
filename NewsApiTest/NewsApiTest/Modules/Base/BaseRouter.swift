//
//  BaseRouter.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit

protocol RouterProtocol {
    func push(viewController: UIViewController)
    func pop()
    func present(viewController: UIViewController, completion: (() -> (Void))?)
    func showError(error: Error)
    func openUrl(url: String)
}

class BaseRouter: RouterProtocol {
    
    var viewController: UIViewController
    var animated = true
    
    init(with viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func push(viewController: UIViewController) {
        self.viewController.navigationController?.pushViewController(viewController, animated: animated)
    }
    
    func pop() {
        viewController.navigationController?.popViewController(animated: animated)
    }
    
    func present(viewController: UIViewController, completion: (() -> (Void))?) {
        self.viewController.present(viewController, animated: animated, completion: completion)
    }
    
    func openUrl(url: String) {
        UIApplication.shared.open(URL(string: url)!, options: [:]) { _ in }
    }
    
    func showError(error: Error) {
        let alertController = UIAlertController(title: R.string.localizable.generalErrorTitle(), message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: R.string.localizable.commonOk(), style: .cancel, handler: nil))
        viewController.present(alertController, animated: true, completion: { })
    }
    
    static func setBaseViewController(_ viewController: UIViewController) {
        let window = getKeyWindow()
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
    }
    
    static func getKeyWindow() -> UIWindow? {
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        return windowScene?.windows.first
    }
    
}
