//
//  BasePresenter.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import Foundation
import Combine

class BasePresenter {
    var disposeBag = Set<AnyCancellable>()
}
