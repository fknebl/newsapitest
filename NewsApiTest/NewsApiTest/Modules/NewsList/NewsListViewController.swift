//
//  NewsListViewController.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit
import Moya
import Combine
import Resolver

class NewsListViewController: BaseViewController {
    
    let CellIdentifier = "CellIdentifier"
    
    var presenter: NewsListPresenterProtocol?
    var viewModel: NewsListModule.ViewModel?
    
    var repo: FeedServiceProtocol {
        Resolver.resolve()
    }
    
    private var articles: [Article] = []
    
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "NewsItem", bundle: nil), forCellReuseIdentifier: CellIdentifier)
            tableView.addSubview(refreshControl)
        }
    }
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView! {
        didSet {
            
        }
    }
    
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel?.$title.assign(to: \.title, on: self).store(in: &disposeBag)
        viewModel?.$articleStatus.sink(receiveValue: { [weak self] status in
            switch status {
            case .empty:
                self?.tableView.isHidden = true
                self?.activityIndicator.stopAnimating()
                self?.refreshControl.endRefreshing()
            case .loaded(let articles):
                self?.articles = articles
                self?.tableView.isHidden = false
                self?.tableView.reloadData()
                self?.refreshControl.endRefreshing()
            case .loading:
                guard let self = self else {
                    return
                }
                if self.articles.isEmpty {
                    self.tableView.isHidden = true
                    self.activityIndicator.startAnimating()
                }
            }
        }).store(in: &disposeBag)

        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        loadArticles()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        loadArticles()
    }
    
    private func loadArticles() {
        presenter?.getArticles()
    }
}

extension NewsListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! NewsItemTableViewCell
        let article = articles[indexPath.row]
        
        cell.itemTitle.text = article.title
        cell.itemDate.text = article.publishedAt.format()
        
        return cell
    }
}

extension NewsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showDetails(for: self.articles[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
