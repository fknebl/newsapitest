//
//  NewsListPresenter.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation

protocol NewsListPresenterProtocol {
    func showDetails(for article: Article)
    func getArticles()
}

class NewsListPresenter: BasePresenter, NewsListPresenterProtocol {
    var viewModel: NewsListModule.ViewModel? {
        didSet {
            guard let _ = viewModel else {
                return
            }
            
            initViewModel()
        }
    }
    let interactor: NewsListInteractorProtocol
    let router: NewsListRouterProtocol
    
    init(interactor: NewsListInteractorProtocol,
         router: NewsListRouterProtocol) {
        self.interactor = interactor
        self.router = router
        super.init()
    }
    
    private func initViewModel() {
        viewModel?.title = "Tesla"
    }
    
    func getArticles() {
        viewModel?.articleStatus = .loading
        interactor.getArticles().sink { [weak self] result in
            switch result {
            case .finished:
                break
            case .failure(let error):
                self?.router.showError(error: error)
            }
        } receiveValue: { [weak self] articles in
            if articles.count > 0 {
                self?.viewModel?.articleStatus = .loaded(articles: articles)
            } else {
                self?.viewModel?.articleStatus = .empty
            }
        }.store(in: &disposeBag)
    }
    
    func showDetails(for article: Article) {
        router.showDetails(for: article)
    }
}
