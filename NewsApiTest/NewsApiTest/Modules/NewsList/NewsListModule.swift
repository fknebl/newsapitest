//
//  NewsListCreator.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit
import Combine
import Resolver

class NewsListModule: ModuleCreator {
    
    enum ArticlesStatus {
        case loading
        case loaded(articles: [Article])
        case empty
    }
    
    class ViewModel {
        @Published var title: String?
        @Published var articleStatus: ArticlesStatus = .loading
    }
    
    let feedRepo: FeedRepoProtocol
    let articleQuery: String
    
    init(query: String, feedRepo: FeedRepoProtocol) {
        self.feedRepo = feedRepo
        articleQuery = query
    }
    
    func create() -> UIViewController {
        let storyboard = R.storyboard.newsList()
        guard let vc = storyboard.instantiateInitialViewController() as? NewsListViewController else {
            fatalError("Failed to instantiate NewsList")
        }
        
        let model = ViewModel()
        vc.viewModel = model
        let interactor = NewsListInteractor(query: articleQuery, feedRepo: feedRepo)
        let router = NewsListRouter(with: vc, imageService: Resolver.resolve())
        let presenter = NewsListPresenter(interactor: interactor, router: router)
        presenter.viewModel = model
        vc.presenter = presenter
        
        return vc
    }
}
