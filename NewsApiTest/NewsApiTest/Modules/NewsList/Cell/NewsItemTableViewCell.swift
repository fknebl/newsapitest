//
//  NewsItemTableViewCell.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import UIKit

class NewsItemTableViewCell: UITableViewCell {
    @IBOutlet var itemTitle: UILabel! {
        didSet {
            itemTitle.font = UIFont.boldSystemFont(ofSize: 18)
            itemTitle.textColor = .black
        }
    }
    @IBOutlet var itemDate: UILabel! {
        didSet {
            itemDate.font = UIFont.systemFont(ofSize: 16)
            itemDate.textColor = .gray
        }
    }
    
}
