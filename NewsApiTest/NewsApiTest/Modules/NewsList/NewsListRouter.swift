//
//  NewsListRouter.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation
import Resolver

protocol NewsListRouterProtocol: RouterProtocol {
    func showDetails(for article: Article)
}

class NewsListRouter: BaseRouter, NewsListRouterProtocol {
    let imageService: ImageFetchServiceProtocol
    
    init(with vc: NewsListViewController, imageService: ImageFetchServiceProtocol) {
        self.imageService = imageService
        super.init(with: vc)
    }
    
    func showDetails(for article: Article) {
        push(viewController: NewsDetailsModule(article: article,
                                               imageService: imageService).create())
    }
}
