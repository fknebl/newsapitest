//
//  NewsListInteractor.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation
import Combine

protocol NewsListInteractorProtocol {
    func getArticles() -> AnyPublisher<[Article], Error>
}

class NewsListInteractor: NewsListInteractorProtocol {
    
    let feedRepo: FeedRepoProtocol
    let query: String
    
    init(query: String, feedRepo: FeedRepoProtocol) {
        self.feedRepo = feedRepo
        self.query = query
    }
    
    func getArticles() -> AnyPublisher<[Article], Error> {
        feedRepo.getArticles(for: query)
    }
    
}
