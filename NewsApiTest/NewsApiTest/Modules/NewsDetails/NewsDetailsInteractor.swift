//
//  NewsDetailsInteractor.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation
import Combine
import UIKit

protocol NewsDetailsInteractorProtocol {
    func getImage(url: String) -> AnyPublisher<UIImage, Error>
}

class NewsDetailsInteractor: NewsDetailsInteractorProtocol {
    let imageService: ImageFetchServiceProtocol
    
    init(imageService: ImageFetchServiceProtocol) {
        self.imageService = imageService
    }
    
    func getImage(url: String) -> AnyPublisher<UIImage, Error> {
        imageService.getImage(from: url)
    }
}
