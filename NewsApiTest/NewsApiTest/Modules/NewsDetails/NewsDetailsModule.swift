//
//  NewsDetailsModule.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit
import Combine

class NewsDetailsModule: ModuleCreator {
    
    enum ImageStatus {
        case loading
        case loaded(image: UIImage)
        case noImage
    }
    
    class ViewModel {
        @Published var articleImageStatus: ImageStatus = .loading
        @Published var articleTitle: String?
        @Published var articleContent: String?
    }
    
    let article: Article
    let imageService: ImageFetchServiceProtocol
    
    init(article: Article, imageService: ImageFetchServiceProtocol) {
        self.article = article
        self.imageService = imageService
    }
    
    func create() -> UIViewController {
        let storyboard = R.storyboard.newsDetails()
        guard let vc = storyboard.instantiateInitialViewController() as? NewsDetailsViewController else {
            fatalError("Failed to instantiate NewsDetails")
        }
        
        let model = ViewModel()
        vc.viewModel = model
        let interactor = NewsDetailsInteractor(imageService: imageService)
        let router = NewsDetailsRouter(with: vc)
        let presenter = NewsDetailsPresenter(interactor: interactor, router: router, article: article)
        presenter.viewModel = model
        vc.presenter = presenter
        
        return vc
    }
}
