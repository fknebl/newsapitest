//
//  NewsDetailsViewController.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import UIKit
import WebKit

class NewsDetailsViewController: BaseViewController {
    
    var viewModel: NewsDetailsModule.ViewModel?
    var presenter: NewsDetailsPresenterProtocol?
    
    @IBOutlet var imageView: UIImageView! {
        didSet {
            
        }
    }
    
    @IBOutlet var content: UILabel! {
        didSet {
            content.font = UIFont.systemFont(ofSize: 19)
        }
    }
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(openTapped(_:)))

        viewModel?.$articleTitle.assign(to: \.title, on: self).store(in: &disposeBag)
        viewModel?.$articleContent.assign(to: \.text, on: content).store(in: &disposeBag)
        viewModel?.$articleImageStatus.sink(receiveValue: { [weak self] status in
            switch status {
            case .noImage:
                self?.activityIndicator.stopAnimating()
            case .loading:
                self?.activityIndicator.startAnimating()
            case .loaded(let image):
                self?.imageView.image = image
                self?.activityIndicator.stopAnimating()
            }
        }).store(in: &disposeBag)
    }
}

extension NewsDetailsViewController {
    
    @objc func openTapped(_ sender: UIBarButtonItem) {
        presenter?.openBrowser()
    }
    
}
