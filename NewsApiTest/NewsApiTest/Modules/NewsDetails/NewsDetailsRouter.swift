//
//  NewsDetailsRouter.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation

protocol NewsDetailsRouterProtocol: RouterProtocol {
    
}

class NewsDetailsRouter: BaseRouter, NewsDetailsRouterProtocol {
    
}
