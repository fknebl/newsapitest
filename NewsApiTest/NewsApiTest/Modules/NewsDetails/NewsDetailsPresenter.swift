//
//  NewsDetailsPresenter.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation

protocol NewsDetailsPresenterProtocol {
    func openBrowser()
}

class NewsDetailsPresenter: BasePresenter, NewsDetailsPresenterProtocol {
    
    let interactor: NewsDetailsInteractorProtocol
    let router: NewsDetailsRouterProtocol
    var viewModel: NewsDetailsModule.ViewModel? {
        didSet {
            guard let _ = viewModel else {
                return
            }
            
            initViewModel()
        }
    }
    
    let article: Article
    
    init(interactor: NewsDetailsInteractorProtocol, router: NewsDetailsRouterProtocol,
         article: Article) {
        self.interactor = interactor
        self.router = router
        self.article = article
    }
    
    func openBrowser() {
        router.openUrl(url: article.url)
    }
    
    private func initViewModel() {
        viewModel?.articleTitle = article.title
        viewModel?.articleContent = article.content
        if let imageUrl = article.urlToImage {
            viewModel?.articleImageStatus = .loading
            interactor.getImage(url: imageUrl).sink { [weak self] result in
                switch result {
                case .finished:
                    break
                case .failure(let error):
                    self?.router.showError(error: error)
                }
            } receiveValue: { [weak self] image in
                self?.viewModel?.articleImageStatus = .loaded(image: image)
            }.store(in: &disposeBag)
        } else {
            viewModel?.articleImageStatus = .noImage
        }
    }
    
}
