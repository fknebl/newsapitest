//
//  AppDelegate+DI.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import Resolver

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        defaultScope = .application
        register {
            FeedService() as FeedServiceProtocol
        }
        register {
            ImageFetchService() as ImageFetchServiceProtocol
        }
        register {
            FeedRepo(feedService: resolve()) as FeedRepoProtocol
        }
    }
}
