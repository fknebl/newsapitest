//
//  Date+Formatting.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import Foundation

extension Date {
    
    func format() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
    
}
