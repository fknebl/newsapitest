//
//  Article.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation

struct Article: Codable {
    var source: ArticleSource
    var author: String?
    var title: String
    var articleDescription: String
    var url: String
    var urlToImage: String?
    var publishedAt: Date
    var content: String
    
    enum CodingKeys: String, CodingKey {
        case source, author, title
        case articleDescription = "description"
        case url, urlToImage, publishedAt, content
    }
}
