//
//  Source.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation

// MARK: - Source
struct ArticleSource: Codable {
    var id: String?
    var name: String
}
