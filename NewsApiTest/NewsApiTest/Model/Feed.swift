//
//  Feed.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 05..
//

import Foundation

struct Feed: Codable {
    var status: String
    var totalResults: Int
    var articles: [Article]
}
