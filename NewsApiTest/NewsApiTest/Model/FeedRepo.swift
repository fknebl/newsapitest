//
//  FeedRepo.swift
//  NewsApiTest
//
//  Created by Ferenc Knebl on 2021. 09. 06..
//

import Foundation
import Combine

protocol FeedRepoProtocol {
    func getArticles(for query: String) -> AnyPublisher<[Article], Error>
}

class FeedRepo: FeedRepoProtocol {
    let feedService: FeedServiceProtocol
    
    init(feedService: FeedServiceProtocol) {
        self.feedService = feedService
    }
    
    func getArticles(for query: String) -> AnyPublisher<[Article], Error> {
        feedService.getFeed(query: query).map { $0.articles }.eraseToAnyPublisher()
    }
}
