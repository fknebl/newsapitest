//
//  NewsApiTestTests.swift
//  NewsApiTestTests
//
//  Created by Ferenc Knebl on 2021. 09. 08..
//

import XCTest
import Combine
@testable import NewsApiTest

class NewsApiTestTests: XCTestCase {

    var feedRepo: FeedRepoProtocol?
    private var disposeBag: Set<AnyCancellable> = []
    
    override func setUpWithError() throws {
        let feedService = MockFeedService()
        feedRepo = FeedRepo(feedService: feedService)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFeedRepo() throws {
        var articles: [Article] = []
        var error: Error?
        let expectation = self.expectation(description: "FeedDownloader")
        
        feedRepo?.getArticles(for: "tesla").sink { result in
            switch result {
            case .finished:
                break
            case .failure(let err):
                error = err
                break
            }
            
            expectation.fulfill()
        } receiveValue: {
            articles = $0
        }.store(in: &disposeBag)
        
        waitForExpectations(timeout: 10)
        
        XCTAssertNil(error)
        XCTAssertEqual(articles.count, 2)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
